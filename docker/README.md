# Docker image for building website

## Put docker image into GitLab registry

* Login: `docker login git.iws.uni-stuttgart.de:4567`
* `docker build -t git.iws.uni-stuttgart.de:4567/dumux-repositories/dumux-website/dumux-website-builder .`
* `docker push git.iws.uni-stuttgart.de:4567/dumux-repositories/dumux-website/dumux-website-builder`

## optional cleanup
* `docker system prune`
* or for just containers: `docker container prune`
* or for just images: `docker image prune`
