FROM ubuntu:jammy
MAINTAINER Timo Koch <timo.koch@iws.uni-stuttgart.de>

# network settings
RUN echo "nameserver 129.69.252.252" > /etc/resolv.conf \
    && echo "nameserver 8.8.8.8" >> /etc/resolv.conf

RUN rm -f /etc/apt/apt.conf.d/docker-gzip-indexes && \
    rm -rf /var/lib/apt/lists/*

# install the basic dependencies
RUN apt-get update && apt-get dist-upgrade --no-install-recommends --yes && \
    DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --yes \
    ca-certificates \
    vim \
    python3-dev \
    git \
    git-lfs \
    pkg-config \
    build-essential \
    gfortran \
    golang-go \
    cmake \
    texlive \
    texlive-latex-extra \
    texlive-science \
    texlive-plain-generic \
    texlive-pictures \
    latexmk \
    graphviz \
    imagemagick \
    ghostscript \
    gnuplot \
    flex \
    bison \
    wget \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# install hugo
# https://gohugo.io/getting-started/installing/
RUN git clone -b v0.92.2 --depth 1 https://github.com/gohugoio/hugo.git \
    && cd hugo && GOBIN=/usr/local/bin/ go install --tags extended && cd .. \
    && rm -rf hugo

# install doxygen
RUN git clone -b Release_1_9_3 --depth 1 https://github.com/doxygen/doxygen.git \
    && mkdir build && cd build && cmake -G "Unix Makefiles" ../doxygen \
    && make && make install && cd .. && rm -rf doxygen build

RUN adduser --disabled-password --home /dumux --uid 50000 dumux
USER dumux:dumux
WORKDIR /dumux
CMD ["/bin/bash"]
