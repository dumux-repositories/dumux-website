#!/bin/bash

umask 022
set -e

# array of branches
dumux_branches=("master" "releases/3.1" "releases/3.2" "releases/3.3" "releases/3.4" "releases/3.5" "releases/3.6" "releases/3.7" "releases/3.8" "releases/3.9")
dumux_branches_with_handbook=("releases/3.1" "releases/3.2" "releases/3.3" "releases/3.4" "releases/3.5" "releases/3.6" "releases/3.7")

toplevel_dir=$PWD

update_website_with_fake_doc() {

	branch=$1

	doxygen_target_dir=${toplevel_dir}/static/docs/doxygen/${branch}
	echo "-- Creating fake doxygen docs for dumux branch ${branch} in ${doxygen_target_dir}..."
	mkdir -p ${doxygen_target_dir}/html
	pushd ${doxygen_target_dir}/html
	if ! test -f index.html; then
		touch index.html
	fi
	popd

	if [[ " ${dumux_branches_with_handbook[*]} " =~ " ${branch} " ]]; then
		handbook_target_dir=${toplevel_dir}/static/docs/handbook/${branch}
		echo "-- Creating fake handbook for dumux branch ${branch} in ${handbook_target_dir}..."
		mkdir -p ${handbook_target_dir}
		pushd ${handbook_target_dir}
		if ! test -f dumux-handbook.pdf; then
			touch dumux-handbook.pdf
		fi
		popd
	fi
}

for current_dumux_branch in "${dumux_branches[@]}"; do
	update_website_with_fake_doc $current_dumux_branch
done
