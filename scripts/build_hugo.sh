#!/bin/bash

set -ex
umask 0022

if test "$#" != "1"
then
  echo "Please provide a targetdir! Exiting."
  exit -1
fi

hugo_target_dir="${1}"
hugo_source_dir=${PWD}

if [ ! -d "${hugo_source_dir}/${hugo_target_dir}" ]; then
  if ! mkdir "${hugo_source_dir}/${hugo_target_dir}"; then
	  echo "Could not create ${hugo_source_dir}/${hugo_target_dir}. Exiting."
	  exit -2
  fi
fi

# run hugo to generate the website
pushd "${hugo_source_dir}"
	hugo -e production --destination "${hugo_source_dir}/${hugo_target_dir}"
popd
