#!/bin/bash

# usage build_documentation.sh
umask 022
set -e

# array of branches
dumux_branches_dune_2_9=("releases/3.1" "releases/3.2" "releases/3.3" "releases/3.4" "releases/3.5" "releases/3.6" "releases/3.7" "releases/3.8" "releases/3.9")
dumux_branches_dune_2_10=("master")
dumux_branches_with_handbook=("releases/3.1" "releases/3.2" "releases/3.3" "releases/3.4" "releases/3.5" "releases/3.6" "releases/3.7")

# testing flags, switch them to yes for shortening runs when testing parts of the script
FLAG_OMIT_PULL=no
FLAG_OMIT_CONFIGURE_REMOVE=no
FLAG_OMIT_BUILD=no
FLAG_OMIT_COPY_RESULTS=no
FLAG_DO_NOT_COPY_ARTIFACTS=no
FLAG_SIMULATE_DOXYGEN_BUILD=no
# caching options
FLAG_OMIT_CLEAN_BUILDDIR=yes
FLAG_OMIT_CLEAN_RESULTS_DIR=yes

toplevel_dir=$PWD

# create build and cache dirs for different setups
build_dir_29=$toplevel_dir/build_dune29
build_cache_dir_29=$toplevel_dir/build_dune29/cache
build_dir_210=$toplevel_dir/build_dune210
build_cache_dir_210=$toplevel_dir/build_dune210/cache

for dir in $build_dir_29 $build_cache_dir_29 $build_dir_210 $build_cache_dir_210
do
	if ! test -d $dir
	then
		mkdir -p $dir
	fi
done


clone_or_update_dune_dumux() {

	# parse function arguments
	build_dir=$1
	build_cache_dir=$2
	dumux_release=$3
	dune_release=$4

	echo "-- Cloning / updating dune and dumux"

	if test $FLAG_OMIT_PULL != "yes"; then
		if pushd $build_dir
		then
			# check version cache flags for the dune modules
			for MOD in common geometry grid localfunctions istl; do
				if test -f ${build_cache_dir}/dune-$MOD-id
				then
					mv ${build_cache_dir}/dune-$MOD-id ${build_cache_dir}/dune-$MOD-id-old
				else
					touch ${build_cache_dir}/dune-$MOD-id-old
				fi
			done

			# get newest release branch of dune modules
			for MOD in common geometry grid localfunctions istl; do
				if ! test -d dune-$MOD
				then
					git clone -b $dune_release https://gitlab.dune-project.org/core/dune-$MOD.git;
					pushd dune-$MOD
					git rev-parse HEAD >> ${build_cache_dir}/dune-$MOD-id
					popd
				else
					pushd dune-$MOD
					git fetch --all
					git checkout $dune_release
					git reset --hard origin/$dune_release
					git rev-parse HEAD >> ${build_cache_dir}/dune-$MOD-id
					popd
				fi
			done

			# get/update dumux
			if ! test -d dumux
			then
				git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
			else
				pushd dumux
				git fetch --all
				git checkout -- doc/doxygen/Doxylocal
				git checkout $dumux_release
				git reset --hard origin/$dumux_release
				popd
			fi

			# reconfigure dune only if some module's version changed or this is a fresh build
			for MOD in common geometry grid localfunctions istl; do
				if ! cmp -s ${build_cache_dir}/dune-$MOD-id ${build_cache_dir}/dune-$MOD-id-old
				then
					test -d dune-$MOD && test -d dune-$MOD/build-cmake && rm -rf dune-$MOD/build-cmake
					echo "-- Configuring dune-$MOD..."
					./dune-common/bin/dunecontrol --only=dune-$MOD --opts=dumux/cmake.opts configure &> ${build_cache_dir}/dune-$MOD.log
				else
					echo "-- Skipped reconfiguring dune-$MOD because version is cached..."
				fi
			done

		else
			exit -10
		fi
		popd
	fi
}

configure_dumux_branch() {

	# parse function arguments
	branch=$1
	previous_commit_sha=$2
	build_dir=$3
	build_cache_dir=$4

	echo ""
	echo "-- Configuring dumux branch $branch..."

	if test $FLAG_OMIT_CONFIGURE_REMOVE != "yes"; then
		if pushd $build_dir
		then
			test -d dumux/build-cmake && rm -rf dumux/build-cmake

			# remove a possible modified copy of Doxylocal
			pushd dumux
			git checkout -- doc/doxygen/Doxylocal
			popd

			pushd dumux
			git checkout $branch
			git reset --hard origin/$branch
			current_commit_sha=$(git rev-parse HEAD)
			popd

			if test $previous_commit_sha = $current_commit_sha
			then
				echo "-- Skipped configuring dumux branch $branch because it is cached..."
				popd
				return 1
			fi

			# replace Doxylocal
			cat ${toplevel_dir}/scripts/Doxylocal >> dumux/doc/doxygen/Doxylocal
			echo "-- Configuring dumux branch $branch..."
			./dune-common/bin/dunecontrol --opts=dumux/cmake.opts --only=dumux configure &> ${build_cache_dir}/dumux/${branch}/dumux-${current_commit_sha}.log
		else
			exit -11
		fi
		popd
	else
		echo "-- Omit configure remove because FLAG_OMIT_CONFIGURE_REMOVE=yes"
	fi
	return 0
}

dumux_make_doc() {

	# parse function arguments
	branch=$1
	build_dir=$2
	build_cache_dir=$3

	pushd ${build_dir}/dumux
	current_commit_sha=$(git rev-parse HEAD)
	popd

	echo "-- Current commit sha: $current_commit_sha"

	if pushd ${build_dir}/dumux/build-cmake; then

		if test $FLAG_SIMULATE_DOXYGEN_BUILD != yes
		then
			echo "Calling make doc for branch ${branch}..."
			# replace on the fly obsolete latex package scrpage2
			if test $branch = "releases/3.1" -o $branch = "releases/3.2" -o $branch = "releases/3.3"
			then
				sed -i -e 's/scrpage2/scrlayer-scrpage/' ../doc/handbook/0_dumux-handbook.tex
			fi

			if ! make doc >& ${build_cache_dir}/dumux/${branch}/make-doc-${current_commit_sha}.log
			then
				rm -f ${build_cache_dir}/dumux/${branch}/lastid
				echo "Dumux branch ${branch} error: make doc failed!"
				exit -13
			fi
			# remove a possible modified local copy of 0_dumux-handbook.tex,
			# have a pristine git-tree
			if test -f ../doc/handbook/0_dumux-handbook.tex; then
				git checkout -- ../doc/handbook/0_dumux-handbook.tex
			fi

			if test -f doc/doxygen/doxyerr.log; then
			    cp doc/doxygen/doxyerr.log ${build_cache_dir}/dumux/${branch}/doxyerr-${current_commit_sha}.log
			fi
			if test -f doc/doxygen/doxygen.log; then
				cp doc/doxygen/doxygen.log ${build_cache_dir}/dumux/${branch}/doxygen-${current_commit_sha}.log
			fi
		else
			if [[ " ${dumux_branches_with_handbook[*]} " =~ " ${branch} " ]]; then
				mkdir -p doc/handbook
				pushd doc/handbook
				if ! test -f 0_dumux-handbook.pdf
				then
					touch 0_dumux-handbook.pdf
				fi
				popd
			fi
			mkdir -p doc/doxygen
			pushd doc/doxygen
			if ! test -d html
			then
				mkdir html
			fi
			touch html/index.html
			touch doxyerr.log doxygen.log
		fi
	else
		exit -12
	fi
	popd
}

update_website_doc() {

	# parse function arguments
	branch=$1
	build_dir=$2

	if test $FLAG_OMIT_COPY_RESULTS != "yes"; then
		doxygen_target_dir=${toplevel_dir}/static/docs/doxygen/${branch}
		echo "-- Copying doxygen for dumux branch ${branch} to ${doxygen_target_dir}..."
		# clear old data before copying the new data
		if test -d ${doxygen_target_dir}; then
			rm -rf ${doxygen_target_dir}
		fi
		mkdir -p ${doxygen_target_dir}
		pushd ${build_dir}/dumux/build-cmake/doc/doxygen
		if test -f html/index.html; then
			mv html/* ${doxygen_target_dir}/
			mv doxyerr.log doxygen.log ${doxygen_target_dir}/
		else
			ls -l html
			echo "-- Error: couldn't find index.html in $(pwd)/htm for dumux branch ${branch}"
			exit -15
		fi
		popd

		if [[ " ${dumux_branches_with_handbook[*]} " =~ " ${branch} " ]]; then
			handbook_target_dir=${toplevel_dir}/static/docs/handbook/${branch}
			echo "-- Copying handboook for dumux branch ${branch} to ${handbook_target_dir}..."
			# clear old data before copying the new data
			if test -d ${handbook_target_dir}; then
				rm -rf ${handbook_target_dir}
			fi
			mkdir -p ${handbook_target_dir}
			pushd ${build_dir}/dumux/build-cmake/doc/handbook
			if test -f 0_dumux-handbook.pdf; then
				mv 0_dumux-handbook.pdf ${handbook_target_dir}/dumux-handbook.pdf
			else
				ls -l .
				echo "-- Error: couldn't find 0_dumux-handbook.pdf in $(pwd) for dumux branch ${branch}"
				exit -16
			fi
			popd
		fi
	fi
}

build_doc_for_branch () {

	# parse function arguments
	current_dumux_branch=$1
	build_dir=$2
	build_cache_dir=$3

	if test -f ${build_cache_dir}/dumux/${current_dumux_branch}/lastid
	then
		read lastid < ${build_cache_dir}/dumux/${current_dumux_branch}/lastid
	else
		lastid=new_build
		test -d ${build_cache_dir}/dumux/${current_dumux_branch} || mkdir -p ${build_cache_dir}/dumux/${current_dumux_branch}
	fi

	if configure_dumux_branch ${current_dumux_branch} ${lastid} ${build_dir} ${build_cache_dir}
	then
		dumux_make_doc ${current_dumux_branch} ${build_dir} ${build_cache_dir}
		echo $current_commit_sha > ${build_cache_dir}/dumux/${current_dumux_branch}/lastid
		update_website_doc ${current_dumux_branch} ${build_dir} ${build_cache_dir}
	else
		echo "-- Skipped building docs for dumux branch ${current_dumux_branch} because the docs are still in the cache..."
	fi
}

cleanup () {
	build_dir=$1
	build_cache_dir=$2

	# remove build directory
	if test $FLAG_OMIT_CLEAN_BUILDDIR != yes
	then
		rm -rf ${build_dir}
		mkdir -p ${build_dir}
	fi

	# remove build cache
	if test $FLAG_OMIT_CLEAN_RESULTS_DIR != yes
	then
		rm -rf ${build_cache_dir}
		mkdir -p ${build_cache_dir}
	fi
}

# update the source repos
cleanup $build_dir_29 $build_cache_dir_29
clone_or_update_dune_dumux $build_dir_29 $build_cache_dir_29 "releases/3.9" "releases/2.9"

for current_dumux_branch in "${dumux_branches_dune_2_9[@]}"
do
	build_doc_for_branch ${current_dumux_branch} $build_dir_29 $build_cache_dir_29
done

cleanup $build_dir_210 $build_cache_dir_210
clone_or_update_dune_dumux $build_dir_210 $build_cache_dir_210 "master" "releases/2.10"

for current_dumux_branch in "${dumux_branches_dune_2_10[@]}"
do
	build_doc_for_branch ${current_dumux_branch} $build_dir_210 $build_cache_dir_210
done
