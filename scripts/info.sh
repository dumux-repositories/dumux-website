#!/bin/bash

umask 022

echo -e "\n--->> Current path  <<---"
pwd

echo -e "\n--->> The PATH variable <<---"
echo $PATH

echo -e "\n--->> The SHELL variable <<---"
echo $SHELL

echo -e "\n--->> User setup: id -a <<---"
id -a

echo -e "\n--->> LaTeX version <<---"
latex --version

echo -e "\n--->> Doxyen version <<---"
doxygen -v

echo -e "\n--->> Hugo version <<---"
hugo version

echo -e "\n--->> Status of git submodules <<---"
git submodule status

echo -e "\n--->> Permissions inside the building directory (ls -al .) <<-- "
ls -al .
