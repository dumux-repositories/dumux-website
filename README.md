## Announcements

If you want to announce something important on the website, such as a new
DuMu<sup>x</sup> course, modify the [announcement snippet](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-website/-/blob/master/content/_index/announcements.md) on the title
page and activate it by setting `disabled = "false"`.

## Local installation

To clone the website repository run

```
git clone --recurse-submodules https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-website.git
```

In order to build to website you also need the Syna theme as a submodule. This is what `--recurse-submodules`
is for. If you already cloned the repository and you switch to a new branch, initialize the submodules again

```
git checkout next
git submodule update --init --recursive
```

If the submodules are not checked out, you will see an empty website.

## Hugo
### Requirements on the version 

Currently the webpage uses Syna v0.17.2:

We require Hugo (extended) version 0.65 or greater.
We also tested with Hugo (extended) 0.73 and 0.92.2.
The version 0.92.2+extended is what the webpage builder dockerimage currently runs with. 

### General remarks about Hugo: 

The page [https://gohugo.io/getting-started/](https://gohugo.io/getting-started/) 
contains much more information than that is covered here. It contains in the installation section
also links to binary versions for installing.


### Installing Hugo from a linux package

Use the package manager of your distribution and install it. Here for example for ubuntu: 
```
sudo apt install hugo
```
Check the version of the package: 
```
hugo version 
```
Make sure its shows a version-number `+extended`. 

### Installing Hugo from a snap 

The simplest way to install Hugo is with the package manager snap, which is available for ubuntu
and some other linux distributions:

```
# make sure that a non-snap package variant of hugo is not installed 
sudo apt remove hugo

# install the snap 
sudo snap install hugo --channel=extended
```

### Running Hugo

Non snap variant runs fine: 
```
hugo 
```
 
In order to run the snap version, it can be necessary to set `enableGitInfo` to false by commandline option, temporarily 
overwriting the setting `config/_default/config.toml. 
```
hugo --enableGitInfo=false
```

alternativly more explicit: 
```
snap run hugo --enableGitInfo=false
```

### Running Hugo variant of the docker builder image for the website
 
This requires that your computer has docker installed and you are allowed to work with docker: 

```
docker run -ti  git.iws.uni-stuttgart.de:4567/dumux-repositories/dumux-website/dumux-website-builder /bin/bash
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-website.git
cd dumux-website
git checkout YOUR_BRANCH 
hugo
exit
```
 
## Running the Gitlab CI on your branch

Inside gitlab tag your dumux-website branch with tagname `next`. Than manually run the CI. 
This runs stage `next` in `.gitlab.yml`. You can download the artifacts and if 
hugo succeeded also run a local webserver on `public` as described below 
in "Merge requests and Gitlab CI". 

## Displaying the website

To locally run the website use

```
hugo server -D
```
or with the explicit snap variant: 

```
snap run hugo --enableGitInfo=false server -D
```

You will see the website in your browser at http://localhost:1313/.
After changes in the source code / markdown files
the website will automatically be update (you might need to refresh the browser).

## Merge requests and Gitlab CI

When opening a merge request the website builder will build the new website and
when the build passed you get green light. For merge requests do not build the
doxygen documentation and the handbook, so these links might not work as expected.
You can preview the website in the state of the merge request by downloading
the artefacts of the build pipeline. They contain a folder `public`. In that
folder run
```
python3 -m http.server 1313
```
to launch a small Python server and then you can look at the website
at http://localhost:1313/. Replace `1313` by any port you want.

## Directory Structure

We're using the standard directory structure using content pages.

```
├─ content/
|  └ _global/ # All global fragments are located in this directory (nav bar, config)
|  └ _index/ # This folder contains snippets for the home page
|  └ about/ # The about page
├─ layouts/ # Extra layout files. You can add hugo templates for new fragments (e.g. look at carousel.html)
├─ static/ # Static files like images, css-styles, other files
├─ themes/ # Location of the Syna theme (do not edit this other than described below)
```

## Syna

We use the Syna theme. It is documented [here](https://syna.okkur.org/docs).
For adding new content, add new markdown files in the `content` folder that contains
all page content. Fill in the header section similar to other files in the same folder.

### For updating syna theme to new version

```
git -C themes/syna checkout v0.17.2
git add themes/syna
git commit -m "[theme] Update syna to new version"
```

## Git submodule cheatsheet (some helpful commands)

### See the list of submodules in the project and their status

```
git submodule status
```

## Hugo cheatsheet (some helpful commands)

### Generate static website in public folder for testing the production environment
```
hugo -e production
cd public
python3 -m http.server 1313
```
