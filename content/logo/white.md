+++
fragment = "item"
#disabled = false
date = "2017-10-04"
weight = 40
background = "dark"
align = "right"

title = "logo (white edition)"
subtitle= "used with dark backgrounds"

# Subtitle pre and post item
#pre = ""
#post = ""

[asset]
  image = "logo_white.svg"
+++
