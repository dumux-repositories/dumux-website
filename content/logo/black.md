+++
fragment = "item"
#disabled = false
date = "2017-10-04"
weight = 30
background = "white"
align = "right"

title = "logo (default)"
subtitle= "used with light backgrounds"

# Subtitle pre and post item
#pre = ""
#post = ""

[asset]
  image = "logo.svg"
+++
