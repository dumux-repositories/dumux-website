+++
fragment = "content"
title = "Get the DuMu<sup>x</sup> logo"
date = "2017-10-05"
weight = 20
+++

This logo or a modified version may be used by anyone to refer to the {{<dumux>}} project
(this does not necessarily indicate endorsement by {{<dumux>}}).
It’s main intention are talks, presentations, websites, print media referring to {{<dumux>}}.
