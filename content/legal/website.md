+++
fragment = "item"
#disabled = true
date = "2019-11-27"
weight = 95
background = "dark"
align = "left"
title = "Website content license"
subtitle = "[CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)"

[asset]
  icon = "fa fa-globe"
+++

The website content (text and images) is licensed under the terms and conditions
of the Creative Commons license [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
unless explicitly stated otherwise (e.g. in the image description).
