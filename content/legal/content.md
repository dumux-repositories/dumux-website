+++
fragment = "item"
#disabled = true
date = "2019-11-27"
weight = 100
#background = ""
align = "left"
title = "Source code license"
subtitle = "[GNU GPL (version 3 or later)](https://www.gnu.org/licenses/gpl-3.0.txt)"

[asset]
  icon = "fa fa-balance-scale"
+++

{{<dumux>}} is licensed under the terms and conditions of the GNU General
Public License (GPL) version 3 or - at your option - any later
version. The GPLv3 can be [read online](https://www.gnu.org/licenses/gpl-3.0.txt)
or in the [LICENSE](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/blob/master/LICENSE.md)
file provided in the topmost directory of the {{<dumux>}} source code tree.

Please note that {{<dumux>}}' license, unlike Dune's, does not feature a
template exception to the GNU GPL. This means that
you must publish any source code which uses any of the {{<dumux>}} header
files if you want to redistribute your program to third parties. If
this is unacceptable to you, please contact us for a commercial
license.

See the [LICENSE](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/blob/master/LICENSE.md)
file for full copying permissions.
