+++
date = "2019-11-27"
fragment = "item"
weight = 150
background = "dark"
align = "left"
title = "Impressum / Legal notice"
+++

[Impressum DE](https://www.uni-stuttgart.de/impressum/) / [Legal notice EN](https://www.uni-stuttgart.de/en/legal-notice//)

#### External links
Some links refer to externally forwarded contents for which neither the {{<dumux>}} Team nor the University of Stuttgart claim ownership.
The responsibility lies with the respective external provider. The external contents were reviewed when setting the link. It cannot be ruled out that the contents are subsequently changed by the respective providers. Should you be of the opinion that any of the external websites to which we are linked infringe upon applicable law or otherwise have inappropriate contents, please notify us accordingly.

#### Icons
[Font Awesome](https://fontawesome.com/)
