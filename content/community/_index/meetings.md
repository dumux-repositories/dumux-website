+++
fragment="item"
title = "DuMu<sup>x</sup> / DUNE user meetings"
weight = 160
background = "light"
align = "left"

[asset]
  icon = "fas fa-users"
+++

User meetings are a great possibility for {{<dumux>}} users from different institutions and countries to meet and exchange knowledge about {{<dumux>}}:

* [6th DUNE User Meeting](https://www.dune-project.org/community/meetings/2023-09-usermeeting/), 2023 in Dresden, Germany
* [5th DUNE User Meeting](https://www.dune-project.org/community/meetings/2018-11-usermeeting), 2018 in Stuttgart, Germany
* [4th DUNE User Meeting](https://www.dune-project.org/community/meetings/2017-03-usermeeting), 2017 in Heidelberg, Germany
* [1st {{<dumux>}} User Meeting](./dumuxmeeting2015), 11. and 12. June, 2015 in Stuttgart, Germany
* [3rd DUNE User Meeting](https://www.dune-project.org/community/meetings/2015-09-usermeeting), 2015 in Heidelberg, Germany
* [2nd DUNE User Meeting](https://www.dune-project.org/community/meetings/2013-09-usermeeting), 2013 in Aachen, Germany
* [1st DUNE User Meeting](https://www.dune-project.org/community/meetings/2010-10-usermeeting), 2010 in Stuttgart, Germany ([Conference Proceedings (Springer)](http://www.springer.com/gp/book/9783642285882))
