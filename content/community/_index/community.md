+++
fragment = "item"
date = "2019-11-27"
weight = 100
title = "The DuMu<sup>x</sup> Community"
+++

{{<dumux>}} is mainly developed and used at the [Department of Hydromechanics and Modelling of Hydrosystems](https://www.iws.uni-stuttgart.de/en/lh2/) at the University of Stuttgart, Germany. However, the number of external developers and users is growing. We offer a transparent development process in which you can observe and participate on our [GitLab instance](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux). Contributions in forms of bug reports, feature requests, and merge request are welcome!
