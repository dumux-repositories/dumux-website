+++
fragment="item"
title = "DuMu<sup>x</sup> GitLab instance / issue tracker"
weight = 135
align = "left"

[asset]
  icon = "fab fa-gitlab"
  url = "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux"
+++

The {{<dumux>}} main repository and many related project are hosted in our
[GitLab instance (click here)](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux).
Contributions in forms of bug reports, feature requests, and merge request are welcome!
You can have a look at (or contribute to) the [current issues here](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/issues).
