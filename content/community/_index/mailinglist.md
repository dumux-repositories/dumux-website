+++
fragment="item"
title = "DuMu<sup>x</sup> mailing list"
weight = 130
background = "white"
align = "left"

[asset]
  icon = "fas fa-fire-extinguisher"
  url = "https://listserv.uni-stuttgart.de/mailman/listinfo/dumux"
+++

The <a href="https://listserv.uni-stuttgart.de/mailman/listinfo/dumux" target="_blank">mailing list (click here to subscribe)</a>
is the easiest way to reach out to the {{<dumux>}} developers.
If you have any {{<dumux>}}-related questions, do not hesitate to send an e-mail to the list&mdash;ideally with a detailed description of your problem ([minimal working example](https://en.wikipedia.org/wiki/Minimal_working_example)), sample code and/or a specific question. The answer or solution to your problem [might be of help to others as well](http://www.catb.org/~esr/faqs/smart-questions.html). Before asking a question, please have a look at the <a href="http://listserv.uni-stuttgart.de/pipermail/dumux/" target="_blank">archive</a> to avoid duplicates.
