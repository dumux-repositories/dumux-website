+++
fragment="item"
title = "DuMu<sup>x</sup> courses"
weight = 150
background = "white"
align = "left"

[asset]
  icon = "fas fa-hand-holding-heart"
  url = "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course"
+++

We offer {{<dumux>}} courses to give potential users the possibility to get started with {{<dumux>}} having direct support of the developers.
The courses are announced on the mailing list, make sure to subscribe if you are interested in a course.
The current course material can be found [here](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course/tree/master).

### Previous courses

* [3rd {{<dumux>}} course](https://www.sfb1313.uni-stuttgart.de/news/SFB-1313-DuMuX-Short-Course/), April 3-5, 2023 in Stuttgart
* Short course [Introduction to Dumux](https://events.interpore.org/event/12/page/86-short-courses#SC1), May 6, 2019 at the InterPore conference in Valencia
* 1st {{<dumux>}} course, July 18-20, 2018 in Stuttgart
