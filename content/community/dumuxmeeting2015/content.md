+++
fragment = "content"
date = "2019-11-27"
weight = 60
align = "left"
title = "DuMu<sup>x</sup> user meeting 2015"
subtitle = "June 11-12, 2015 in Stuttgart, Germany"
+++

### Organisation
We plan to have the {{<dumux>}} user meeting in
Room 1.002 ("Seminarraum 2") at the LH2 in

    Pfaffenwaldring 61
    70569 Stuttgart
    Germany

There is the Commundo hotel next to the campus, but it is booked out for the night before the meeting.
We recommend a room in the inner city of Stuttgart close to an S-Bahn station or in the district
Vaihingen. You can search, for example, the [Stuttgart Tourist Center](https://int.stuttgart-tourist.de/en).

Organizers are Bernd Flemisch and Christoph Grüninger please contact us whenever you have a question.

### Program and Presentation Slides

##### Thursday, 11/6/2015

<table class="table table-hover">
  <colgroup>
    <col span="1" style="width: 15%;">
    <col span="1" style="width: 85%;">
  </colgroup>
  <tr>
    <td>9:00 - 10:30</td>
    <td>
      B. Flemisch: <i>Introduction, history, current state</i>
        {{<download-link link="/docs/usermeeting2015/dumux_introduction.pdf" text="PDF">}}
      and <i>User Poll</i>
        {{<download-link link="/docs/usermeeting2015/dumux_poll.pdf" text="PDF">}}
    </td>
  </tr>
  <tr>
    <td></td><td>Coffee</td>
  </tr>
  <tr>
    <td>11:00 - 12:30</td><td>User Presentations</td>
  </tr>
  <tr>
    <td></td>
    <td>
      B. Hagemann: <i>Hydrodynamic and bio-chemical effects during underground hydrogen storage</i>
        {{<download-link link="/docs/usermeeting2015/hagemann_dumux.pdf" text="PDF">}}
      <br>
      J. Tecklenburg: <i>Two phase flow in fractured porous media</i>
      <br>
      T.D. Ngo: <i>3D parallel simulation of CO2 injection into the SPE model using {{<dumux>}}</i>
        {{<download-link link="/docs/usermeeting2015/NGO_DUM_15.pdf" text="PDF">}}
    </td>
  </tr>
  <tr>
    <td></td><td>Lunch</td>
  </tr>
  <tr>
    <td>14:00 - 15:30</td><td>LH2 Presentations</td>
  </tr>
  <tr>
    <td></td><td>
      H. Class, C. Grüninger: <i>The dumux-lecture module and ViPLab: Using {{<dumux>}} for education</i>
        {{<download-link link="/docs/usermeeting2015/DUM_dumux-lecture.pdf" text="PDF">}}
      <br>
      T. Koch, N. Schröder: <i>Coupling domains of different space dimension using dune-grid-glue and dune-foamgrid</i>
        {{<download-link link="/docs/usermeeting2015/DumuxUserMeeting1d3d.pdf" text="PDF">}}
      <br>
      B. Becker: <i>Coupling of a vertical-equilibrium model to a full-dimensional model</i>
        {{<download-link link="/docs/usermeeting2015/DUMUX-presentation_Beatrix.pdf" text="PDF">}}
    </td>
  </tr>
  <tr>
    <td></td><td>Coffee</td>
  </tr>
  <tr>
    <td>16:00 - 18:00</td><td>User Presentations</td>
  </tr>
  <tr>
    <td></td><td>
      D. Baum: <i>Modeling of low-temperature fuel cells</i>
      <br>
      A. Schnepf: <i>Models of root growth, water and solute uptake: System of equations aimed for {{<dumux>}} implementation</i>
        {{<download-link link="/docs/usermeeting2015/schnepf.pdf" text="PDF">}}
      <br>
      A. Kunkel: <i>The fate of pesticides - Multidimensional modelling of water and solute transport in unsaturated soil, and uptake through roots</i>
    </td>
  </tr>
  <tr>
    <td>19:00</td><td>Dinner</td>
  </tr>
</table>

##### Friday, 12/6/2015

<table class="table table-hover">
  <colgroup>
    <col span="1" style="width: 15%;">
    <col span="1" style="width: 85%;">
  </colgroup>
  <tr>
    <td>9:00 - 10:30</td><td>LH2 presentations</td>
  </tr>
  <tr>
    <td></td><td>
      B. Flemisch: <i>The dumux-pub module</i>
        {{<download-link link="/docs/usermeeting2015/dumux_pub.pdf" text="PDF">}}
      <br>
      T. Koch, M. Schneider: <i>Adaptive grids in {{<dumux>}}</i>
        {{<download-link link="/docs/usermeeting2015/DuMuXUserMeeting_AdaptiveGrids_MartinSchneider.pdf" text="PDF">}}
      <br>
      T. Fetzer, C. Grüninger: <i>Free flow / porous-medium flow coupling</i>
        {{<download-link link="/docs/usermeeting2015/FetzerGrueninger-FreeFlowPorousMediaCoupling_homepage.pdf" text="PDF">}}
    </td>
  </tr>
  <tr>
    <td></td><td>Coffee</td>
  </tr>
  <tr>
    <td>11:00 - 12:00</td><td>LH2 Presentations</td>
  </tr>
  <tr>
    <td></td><td>
      A. Kissinger: <i>Setup of realistic geological models module</i>
        {{<download-link link="/docs/usermeeting2015/2015_Kissinger_DumuxUserMeeting.pdf" text="PDF">}}
      <br>
      K. Weishaupt: <i>Field Scale Application Case: Steam Injection into Saturated Soil</i>
        {{<download-link link="/docs/usermeeting2015/DumuxUserMeeting2015_Kilian.pdf" text="PDF">}}
      <br>
      A. Tatomir: <i>Modeling flow in fractured porous media</i>
        {{<download-link link="/docs/usermeeting2015/Dumux_FromDiscreteToContinuumComplete_give.pdf" text="PDF">}}
      <br>
      M. Beck: <i>Geomechanical Models in {{<dumux>}}</i>
        {{<download-link link="/docs/usermeeting2015/DumuxUserMeeting15_Beck.pdf" text="PDF">}}
    </td>
  </tr>
  <tr>
    <td>12:00 - 12:30</td><td>
      B. Flemisch: Future Development Discussion
        {{<download-link link="/docs/usermeeting2015/dumux_future.pdf" text="PDF">}}
      and Closing
    </td>
  </tr>
  <tr>
    <td></td><td>Lunch</td>
  </tr>
</table>
