+++
title = "Why is the Newton text output scrambled in my log file?"
weight = 10
+++

If you write or pipe the simulation output to a file,
strange symbols appear for before every Newton iteration.
This is because the default output scheme uses a dynamic output, which
deletes lines in the terminal. This makes it possible to track the status
(Assemble, Solve, Update) life. You can disable the dynamic output
by setting

```
[Newton]
EnableDynamicOutput = false
```

in the input file.
