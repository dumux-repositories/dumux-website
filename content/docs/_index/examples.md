+++
fragment = "item"
title = "DuMu<sup>x</sup> examples"
weight = 30
align = "right"
background = "white"

[asset]
  image = "code-examples.gif"
+++

There is a collection of documented code examples available in the {{<dumux>}} repository.

<a type="button" class="btn btn-primary btn-lg" href="https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/blob/master/examples/README.md">
  <i class="fas fa-external-link-alt"></i> Open {{<dumux>}} examples
</a>
