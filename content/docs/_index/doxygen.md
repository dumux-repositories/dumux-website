+++
fragment = "item"
title = "Documentation pages"
weight = 29
align = "left"
background = "white"

[asset]
  icon = "fas fa-project-diagram"
+++

<a type="button" class="btn btn-primary btn-lg" href="/docs/doxygen/master/" target="_blank">
  <i class="fas fa-external-link-alt"></i> Documentation (latest)
</a>

<a type="button" class="btn btn-primary btn-lg" href="/docs/doxygen/master/installation.html" target="_blank">
  <i class="fas fa-external-link-alt"></i> Installation instructions
</a>

<a type="button" class="btn btn-primary btn-lg" href="/docs/doxygen/master/getting-started.html" target="_blank">
  <i class="fas fa-external-link-alt"></i> Getting started
</a>
<br><br>

Older versions of the documentation pages can be found here:

<a type="button" class="btn btn-primary btn-sm" href="/docs/doxygen/releases/3.9/" target="_blank"><i class="fas fa-external-link-alt"></i> v3.9 (stable) </a>
<a type="button" class="btn btn-primary btn-sm" href="/docs/doxygen/releases/3.8/" target="_blank"><i class="fas fa-external-link-alt"></i> v3.8 (stable) </a>
<a type="button" class="btn btn-primary btn-sm" href="/docs/doxygen/releases/3.7/" target="_blank"><i class="fas fa-external-link-alt"></i> v3.7 (stable) </a>
<a type="button" class="btn btn-primary btn-sm" href="/docs/doxygen/releases/3.6/" target="_blank"><i class="fas fa-external-link-alt"></i> v3.6 (stable) </a>
<a type="button" class="btn btn-primary btn-sm" href="/docs/doxygen/releases/3.5/" target="_blank"><i class="fas fa-external-link-alt"></i> v3.5 (stable) </a>
<a type="button" class="btn btn-primary btn-sm" href="/docs/doxygen/releases/3.4/" target="_blank"><i class="fas fa-external-link-alt"></i> v3.4 (stable) </a>
<a type="button" class="btn btn-primary btn-sm" href="/docs/doxygen/releases/3.3/" target="_blank"><i class="fas fa-external-link-alt"></i> v3.3 (stable) </a>
<a type="button" class="btn btn-primary btn-sm" href="/docs/doxygen/releases/3.2/" target="_blank"><i class="fas fa-external-link-alt"></i> v3.2 (stable) </a>
<a type="button" class="btn btn-primary btn-sm" href="/docs/doxygen/releases/3.1/" target="_blank"><i class="fas fa-external-link-alt"></i> v3.1 (stable) </a>
