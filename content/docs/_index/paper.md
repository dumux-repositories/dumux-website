+++
fragment = "item"
title = "DuMu<sup>x</sup> publications"
weight = 40
align = "left"

[asset]
  icon = "fas fa-file"
+++

The following two scientific publications describe some of the concepts behind {{<dumux>}}.

Koch, Timo, Dennis Gläser, Kilian Weishaupt, et al. (2020) [{{<dumux>}} 3 -- an Open-Source Simulator for Solving Flow and Transport Problems in Porous Media with a Focus on Model Coupling](https://doi.org/10.1016/j.camwa.2020.02.012). Computers and Mathematics with Applications.

<a type="button" class="btn btn-primary btn-sm" href="https://www.sciencedirect.com/science/article/pii/S0898122120300791/pdfft?isDTMRedir=true&download=true"><i class="fas fa-download"></i> PDF </a>
<a type="button" class="btn btn-primary btn-sm" href="/bib/dumux2020.bib"><i class="fas fa-download"></i> BibTeX</a>

Flemisch, B., Darcis, M., Erbertseder, K., Faigle, B., Lauser, A., Mosthaf, K., Müthing, S., Nuske, P., Tatomir, A., Wolff, M. and R. Helmig (2011): [DuMux: DUNE for Multi-{Phase, Component, Scale, Physics, ...} Flow and Transport in Porous Media](https://doi.org/10.1016/j.advwatres.2011.03.007). Advances in Water Resources.

<a type="button" class="btn btn-primary btn-sm" href="/docs/papers/dumux_awrpaper.pdf"><i class="fas fa-download"></i> PDF </a>
<a type="button" class="btn btn-primary btn-sm" href="/bib/dumux2011.bib"><i class="fas fa-download"></i> BibTeX </a>
