+++
fragment = "item"
title = "DuMu<sup>x</sup> handbook"
weight = 50
align = "left"

[asset]
  icon = "fas fa-book-open"
+++

Note that release 3.7 is the last release containing a handbook PDF. The handbook content
has been transferred to the online documentation (doxygen/html) and we no longer publish a PDF handbook.
Click on the version to download the handbook PDF document published for the corresponding release.

<a type="button" class="btn btn-primary btn-sm" href="/docs/handbook/releases/3.7/dumux-handbook.pdf"><i class="fas fa-download"></i> v3.7 (stable) </a>
<a type="button" class="btn btn-primary btn-sm" href="/docs/handbook/releases/3.6/dumux-handbook.pdf"><i class="fas fa-download"></i> v3.6 (stable) </a>
<a type="button" class="btn btn-primary btn-sm" href="/docs/handbook/releases/3.5/dumux-handbook.pdf"><i class="fas fa-download"></i> v3.5 (stable) </a>
<a type="button" class="btn btn-primary btn-sm" href="/docs/handbook/releases/3.4/dumux-handbook.pdf"><i class="fas fa-download"></i> v3.4 (stable) </a>
<a type="button" class="btn btn-primary btn-sm" href="/docs/handbook/releases/3.3/dumux-handbook.pdf"><i class="fas fa-download"></i> v3.3 (stable) </a>
<a type="button" class="btn btn-primary btn-sm" href="/docs/handbook/releases/3.2/dumux-handbook.pdf"><i class="fas fa-download"></i> v3.2 (stable) </a>
<a type="button" class="btn btn-primary btn-sm" href="/docs/handbook/releases/3.1/dumux-handbook.pdf"><i class="fas fa-download"></i> v3.1 (stable) </a>
