+++
fragment = "faq"
title = "Collection of frequently asked questions (FAQ)"
weight = 100
align = "left"
background = "dark"
disabled = "true"

[asset]
  icon = "fas fa-question-circle"
+++
