+++
fragment = "item"
title = "DuMu<sup>x</sup> Wiki"
weight = 60
align = "left"
background = "white"

[asset]
  icon = "fas fa-question-circle"
  url = "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/wikis/home"
+++

The [{{<dumux>}} Wiki](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/wikis/home)
contains some tips, tricks and code snippets for common problems.
The Wiki is still in its beginnings. We are happy about contributed content and suggestions.
