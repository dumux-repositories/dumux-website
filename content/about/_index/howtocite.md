+++
date = "2020-02-04"
fragment = "item"
weight = 80
background = "dark"
align = "left"
title = "How to cite DuMu<sup>x</sup>"
subtitle = "Publications and software releases"

[asset]
  icon = "fas fa-quote-right"
+++

{{<dumux>}} is a research code. If you are using {{<dumux>}} in a scientific publication, please cite one of the following references:

__Publications (journal articles):__

<i class="fas fa-file"></i> Koch, T., Gläser, D., Weishaupt, K., et al. (2020) [{{<dumux>}} 3 -- an open-source simulator for solving flow and transport problems in porous media with a focus on model coupling](https://doi.org/10.1016/j.camwa.2020.02.012). Computers and Mathematics with Applications.

<a type="button" class="btn btn-primary" href="https://www.sciencedirect.com/science/article/pii/S0898122120300791/pdfft?isDTMRedir=true&download=true"><i class="fas fa-download"></i> PDF </a>
<a type="button" class="btn btn-primary" href="/bib/dumux2020.bib"><i class="fas fa-download"></i> BibTeX</a>

<i class="fas fa-file"></i> Flemisch, B., Darcis, M., Erbertseder, K., Faigle, B., Lauser, A., Mosthaf, K., Müthing, S., Nuske, P., Tatomir, A., Wolff, M. and R. Helmig (2011): [{{<dumux>}}: DUNE for Multi-{Phase, Component, Scale, Physics, ...} Flow and Transport in Porous Media](https://doi.org/10.1016/j.advwatres.2011.03.007). Advances in Water Resources.

<a type="button" class="btn btn-primary" href="/docs/papers/dumux_awrpaper.pdf"><i class="fas fa-download"></i> PDF </a>
<a type="button" class="btn btn-primary" href="/bib/dumux2011.bib"><i class="fas fa-download"></i> BibTeX </a>

Up to 3.5, all releases have been published at Zenodo. From 3.6, the releases are published at DaRUS.

__Zenodo software releases:__
<div class="dropdown">
  <a class="btn btn-primary dropdown-toggle" role="button" data-toggle="dropdown">
    Select {{<dumux>}} version to open Zenodo page
  </a>
  <div class="dropdown-menu">
    <a class="dropdown-item" href="https://doi.org/10.5281/zenodo.2479594" target="_blank">Version 3</a>
    <a class="dropdown-item" href="https://doi.org/10.5281/zenodo.1115499" target="_blank">Version 2</a>
  </div>
</div>
<br>

__DaRUS software releases:__

<a type="button" class="btn btn-primary" href="https://darus.uni-stuttgart.de/dataverse/iws_lh2_dumux"> DaRUS </a>
