+++
fragment = "content"
#disabled = true
date = "2017-10-05"
weight = 50
title = ""
align = "left"

[sidebar]
  title = "On this page"
  align = "left"
  #sticky = true # Default is false
  content = """
[About](#)  
[How to cite](#howtocite)  
[Other modules](#items)  
[License](#license)  
"""
+++

### {{<dumux>}} is

* short for _Dune for Multi-{Phase, Component, Scale, Physics, ...} flow and transport in porous media_
* a free and open-source simulator for flow and transport processes in porous media
* a _research code_ written in C++
* based on [Dune (Distributed and Unified Numerics Environment)](http://dune-project.org/)
* a Dune user module in the [Dune environment](https://www.dune-project.org/groups/user/)

Its main intention is to provide a sustainable and consistent framework for the implementation and application of porous media model concepts and constitutive relations.
It has been successfully applied to

* gas (CO<sub>2</sub>, H<sub>2</sub>, CH<sub>4</sub>, ...) storage scenarios
* environmental remediation problems
* transport of therapeutic agents through biological tissue
* root-soil interaction
* subsurface-atmosphere coupling (Navier Stokes / Darcy)
* pore-network modelling
* flow and transport in fractured porous media
* and more...

Have a look at the lists of
[scientific articles](https://puma.ub.uni-stuttgart.de/group/dumux/dumuxarticle?resourcetype=publication&items=1000&sortPage=year)
and
[PhD theses](https://puma.ub.uni-stuttgart.de/group/dumux/dumuxphd?resourcetype=publication&items=1000&sortPage=year)
that have been achieved with the help of {{<dumux>}}.

The development of {{<dumux>}} started in January 2007 at the University of Stuttgart in the
[Department of Hydromechanics and Modelling of Hydrosystems](https://www.iws.uni-stuttgart.de/en/lh2/index.html).
The [main {{<dumux>}} repository](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git) is currently hosted via GitLab at the IWS, University of Stuttgart.

{{<dumux>}} is associated partner of the [OPM (Open Porous Media)](https://opm-project.org/) initiative.

The development of {{<dumux>}} has been and is often directly or indirectly supported financially through research projects.
In particular, we want to acknowledge the following institutions for supporting projects involving {{<dumux>}}:

<div class="row">
  <div class="col-sm">
    <div class="rounded-circle view overlay zoom dumux-logo-cirle" onclick="window.open('https://www.iws.uni-stuttgart.de/lh2/');" style="background-image:url(/images/lh2.jpeg);"></div>
  </div>
  <div class="col-sm">
    <div class="rounded-circle dumux-logo-cirle" onclick="window.open('https://www.sfb1313.uni-stuttgart.de/');" style="background-image:url(/images/sfb1313.png);"></div>
  </div>
  <div class="col-sm">
    <div class="rounded-circle dumux-logo-cirle" onclick="window.open('https://www.simtech.uni-stuttgart.de/');" style="background-image:url(/images/simtech.jpg);"></div>
  </div>
  <div class="col-sm">
    <div class="rounded-circle dumux-logo-cirle" onclick="window.open('https://www.dfg.de/en/index.jsp');" style="background-image:url(/images/dfg.png);"></div>
  </div>
  <div class="col-sm">
    <div class="rounded-circle dumux-logo-cirle" onclick="window.open('https://mwk.baden-wuerttemberg.de/de/startseite/');" style="background-image:url(/images/logo-bawue.png);"></div>
  </div>
  <div class="col-sm">
    <div class="rounded-circle dumux-logo-cirle" onclick="window.open('https://www.baw.de/EN/Home/home_node.html');" style="background-image:url(/images/baw.png);"></div>
  </div>
  <div class="col-sm">
    <div class="rounded-circle dumux-logo-cirle" onclick="window.open('https://cordis.europa.eu/project/id/801133');" style="background-image:url(/images/eu-flag190.jpeg);"></div>
  </div>
</div>

<style>
.dumux-logo-cirle {
  height: 100px;
  width: 100px;
  align: center;
  background-position: center;
  background-size: 90% auto;
  background-repeat: no-repeat;
  background-color: white;
}
.dumux-logo-cirle:hover {
  background-size: 100% auto;
}
</style>
