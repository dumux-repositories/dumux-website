+++
#disabled = false
date = "2017-10-04"
weight = 100
background = "light"
align = "right"
title = "dumux-pub"

[asset]
  icon = "fas fa-beer"
  url = "https://git.iws.uni-stuttgart.de/dumux-pub"
+++

Archives of published research results achieved with the help of {{<dumux>}}.
Each archive consist of a DUNE module that allows to replicate the results of a corresponding publication (journal articles, PhD, Master's, and Bachelor's theses).
Each module contains an installation instruction and a list of dependencies with the relevant versions.
All modules are hosted [here](https://git.iws.uni-stuttgart.de/dumux-pub).
