+++
#disabled = false
date = "2017-10-04"
weight = 100
background = "white"
align = "right"
title = "dumux-course"

[asset]
  icon = "fas fa-graduation-cap"
  url = "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course/blob/master/README.md"
+++

A module containing tutorials for beginners, documented examples, task descriptions and solutions. Can be used without prior knowledge of {{<dumux>}}.
The module also contains the slides from the last {{<dumux>}} course event. The source code is up-to-date with the latest {{<dumux>}} release.
The module can be found [here](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course/blob/master/README.md).
