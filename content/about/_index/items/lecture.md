+++
#disabled = false
date = "2017-10-04"
weight = 100
background = "white"
align = "right"
title = "dumux-lecture"

[asset]
  icon = "fas fa-chalkboard"
  url = "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-lecture/blob/master/README.md"
+++

All example applications used in the Master's-level lectures offered by the Department of Hydromechanics and Modelling of Hydrosystems at
the University of Stuttgart. Each application is contained in its own folder with detailed explanations
in the description subfolder. This subfolder also contains LaTeX source files that can be used for generating a PDF.
Visit [dumux-lecture](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-lecture/blob/master/README.md) for more information.
