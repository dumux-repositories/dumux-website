+++
date = "2017-10-05"
title = "Water transport in the vadose zone"
subtitle = "Maize root water uptake simulation"
weight = 40
background = "white"

[header]
  image = "gallery/maizewateruptake.gif"
+++

Simulation of root water uptake by a maize root system in unsaturated soil using a mixed-dimension model.
The maize root system has been generated with the open-source root growth model [CRootBox](https://plant-root-soil-interactions-modelling.github.io/CRootBox/). The domain has periodic boundaries in horizontal direction simulating
a field of maize plants. The simulation setup is adapted from [Leitner et al. (2014)](https://doi.org/10.1016/j.fcr.2014.05.009).
The mathematical model and the numerical method and is described in [Koch et al. (2018)](https://doi.org/10.2136/vzj2017.12.0210).
