+++
date = "2017-10-05"
title = "Brain tissue perfusion"
weight = 38
subtitle = "Contrast agent perfusion simulation for DSC-MRI"
background = "white"

[header]
  image = "gallery/signal_small.gif"
+++

Simulation of the local nuclear magnetic response of a brain tissue sample
during contrast agent administration. The model is described and analysed
in [Koch et al. (2020)](https://doi.org/10.1002/cnm.3298). It allows to
estimate the capillary wall diffusion coefficient and the contrast agent leakage
if the blood-brain-barrier is damaged (e.g. multiple sclerosis, glioblastoma).
