+++
date = "2020-07-29"
title = "Evaporation including solar radiation"
subtitle = "Coupled Darcy and Navier-Stokes flow"
weight = 46
background = "white"

[header]
  image = "gallery/radiation.gif"
+++

Water vapour is evaporating from a partially saturated sandy soil. The wavy surface structure mimics surface structures that can occur in a tilled soil. The simulation starts at 6 in the morning when the sun rises on the left side of the domain. Until noon, the left sides of the hills heat up due to the solar radiation while the right sides are in the shade and stay cooler. In the afternoon, the right sides of the hills heat up, leading to locally increased evaporation rates. After sunset the porous medium cools down due to evaporative cooling and conductive heat exchange with the cooler atmosphere.

Simulation results are published in [Heck et al. (2020)](https://doi.org/10.1029/2020WR027332). The source code is openly available at [dumux-pub/heck2020a](https://git.iws.uni-stuttgart.de/dumux-pub/heck2020a).
