+++
date = "2017-10-05"
title = "Fractured rock systems"
subtitle = "A discrete fracture model based on the box scheme"
weight = 52
background = "white"

[header]
  image = "gallery/fractures.gif"
+++

Infiltration of CO<sub>2</sub> through an interconnection network of fractures in a fully water saturated medium.
Simulation based on a vertex-centred finite volume scheme (Box)
with auxiliary terms in the balance equations to account for fracture flow (Box-DFM).
The scheme is suitable for conducting fractures.
