+++
date = "2022-04-19"
title = "Thermally enhanced soil remediation"
subtitle = ""
weight = 45
background = "white"

[header]
  image = "gallery/remediation.png"
+++

These figures show two different injection strategies for thermally enhanced remediation of NAPL-contaminated 
heterogeneous sands. The main difference in injecting pure steam (left) on the one hand or a steam/air 
mixture (right) on the other hand is that addition of air involves a non-condensible gas. Although having 
much 
lower heat capacity than steam, the additional air allows for a control of temperature and can prevent 
re-mobilization of contaminants and a shifting of the contamination into deeper regions where it is even 
harder to recover. This can be seen in comparing the plots of water saturation, NAPL saturation, temperature, 
and the concentration of evaporated contaminant (in this case: mesitylene) in these plots.

The source code for this example uses the 3p3cni model and is provided with the 
[dumux-lecture](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-lecture) module, which serves as 
support for teaching in courses on multiphase modelling in porous media.
