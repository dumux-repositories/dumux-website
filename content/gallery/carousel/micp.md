+++
date = "2019-04-25"
title = "Permeability around a fracture modified by microbially-induced calcite precipitation"
weight = 50
background = "white"

[header]
  image = "gallery/permeabilityradial3dfracturemicp.png"
+++

The figure shows the final permeability of a microbially induced calcite precipitation (MICP) application after 24 cycles of biomineralization medium injections in a radially-symmetric domain with a highly permeable region representing a fracture. The setup corresponds to a field experiment described in [Phillips et al. (2016)](https://pubs.acs.org/doi/abs/10.1021/acs.est.5b05559). The biomineralization medium is injected from a central well at the inner radius of the simulation domain directly into the highly permeable layer. The simulation setup and additional results are described in more detail in [Cunningham et al. (2018)](https://link.springer.com/article/10.1007%2Fs10596-018-9797-6) and in [Hommel et al. (2016)](https://elib.uni-stuttgart.de/handle/11682/8787). The code is available in the dumux-pub modules [dumux-pub/Hommel2018a](https://git.iws.uni-stuttgart.de/dumux-pub/hommel2018a.git) or [dumux-pub/Hommel2016a](https://git.iws.uni-stuttgart.de/dumux-pub/Hommel2016a.git).
