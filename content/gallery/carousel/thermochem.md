+++
date = "2020-07-01"
title = "Thermochemical heat storage"
subtitle = "Indirectly charged reactor concept "
weight = 45
background = "white"

[header]
  image = "gallery/thermochem.gif"
+++

The figure shows the temperature distribution in an indirectly heated reactor for thermochemical heat storage during discharge. The setup is based on the experiments described by [Schmidt et al. (2017)](https://doi.org/10.1016/j.apenergy.2017.06.063).
The model consists of two domains: 
(1) On the top, there is the 1-dimensional heat transfer channel with a single phase air flux.
(2) The porous reaction bed is solved in two dimensions. 
The two domains of different dimension are coupled by a convective heat flux. Thus, the air flow in the heat transfer channel removes the heat released in the reactive bed and keeps the reaction running until complete conversion.
Simulation results are published in [Seitz et al. (2021)](https://doi.org/10.3390/app11020682). The pub-module containing the source code is available [here](https://git.iws.uni-stuttgart.de/dumux-pub/seitz2020a).

