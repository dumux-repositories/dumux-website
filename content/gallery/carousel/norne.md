+++
date = "2020-06-30"
title = "Norne reservoir oil production"
subtitle = "Two-phase flow in porous media"
weight = 36
background = "white"

[header]
  image = "gallery/norne_sandp_plain.jpg"
+++

Oil production simulation based on the [Norne data set](https://github.com/OPM/opm-data).
Two injection wells inject water into an initially oil-saturated formation with isotropic
heterogeneous permeability field. Two production wells produce oil. The grid is represented
by a [corner-point grid](https://github.com/OPM/opm-grid). Injection rates are approximated
with a Peaceman well model. The figure is modified after [Koch, Gläser, Weishaupt et al. (2020)](https://doi.org/10.1016/j.camwa.2020.02.012) / [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/) and is based on the simulation data of [Schneider (2019)](http://dx.doi.org/10.18419/opus-10416).
