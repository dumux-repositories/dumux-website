+++
date = "2020-03-11"
title = "Two-phase flow in fractured porous media"
subtitle = "Synthetic core sample experiment"
weight = 41
background = "white"

[header]
  image = "gallery/fracturedsample.gif"
+++

The simulation shown in this figure mimics a two-phase flow experiment on a core sample
of fractured rock. Shown are the saturation and velocity of hydrogen as it migrates
through a synthetic sample that contains a number of elliptical and randomly distributed fractures
(generated with the fracture network generator [Frackit](https://git.iws.uni-stuttgart.de/tools/frackit)).
In this model, the fractures are represented by two-dimensional geometries, and it is able to handle
both highly-permeable as well as blocking fractures. In the setting shown in the figure, the fractures
are much more permeable than the surrounding medium, which causes hydrogen to be transported rapidly
through the fracture network.
