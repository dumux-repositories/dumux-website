+++
date = "2017-10-05"
title = "Root-soil interaction"
subtitle = "Root water uptake simulation"
weight = 100
background = "white"

[header]
  image = "gallery/rootsoil.png"
+++

Simulation of root water uptake by a lupine root system. Water flow in the surrounding soil
is visualised by arrows. The lupine root system has been reconstructed from MRI data ([Schröder (2014)](http://hdl.handle.net/2128/5912)).
In the simulation, the root system geometry is approximated by a network of line segment (local root centre-line),
visualised as tubes extruded with the local root radius. The root system is embedded into the
three-dimensional soil domain by means of a mixed-dimension method.
The mathematical model and the numerical method and is described in [Koch et al. (2018)](https://doi.org/10.2136/vzj2017.12.0210).
