+++
date = "2020-08-11"
title = "Density-driven instabilities"
subtitle = "Investigations for saturated porous media"
weight = 46
background = "white"

[header]
  image = "gallery/instabilities.gif"
+++

Water is evaporating at the top of the domain from porous medium fully saturated with a saline solution. The bottom of the domain is connected to a large reservoir of saline solution which keeps the domain saturated. As a result of the evaporative water loss the salt concentration and also the liquid density at the top rises. This leads to an unstable layering as fluids of higher densities tend to sink down due to gravitational forces. Depending on different parameters as e.g. the permeability of the porous medium these instabilities occure or the salt concentration reaches the solubility limit and salt precipitates. Details on the setup and results of the simulations are published in [Bringedal et al. (2022)](https://doi.org/10.1007/s11242-022-01772-w). The [source code](https://git.iws.uni-stuttgart.de/dumux-pub/bringedal2021a) and the [simulation results](https://doi.org/10.18419/darus-2578) are openly available.
