+++
date = "2020-06-30"
title = "Fluid flow around the DuMu<sup>x</sup> logo"
subtitle = "Navier-Stokes equations"
weight = 35
background = "white"

[header]
  image = "gallery/dumux_logo_navierstokes_3D.gif"
+++

The simulation shows fluid flow around the {{<dumux>}} logo visualised by streamlines.
The computational grid is created with the {{<dumux>}} binary image grid reader
which makes use of the Dune grid interface implementation [dune-subgrid](https://www.dune-project.org/modules/dune-subgrid/).
The flow solver is based on Newton's method and the Navier-Stokes equations are discretised
with an implicit Euler scheme in time and a staggered grid finite volume scheme in space.
