+++
date = "2020-07-09"
title = "River engineering applications"
subtitle = "Shallow water equations (fully implicit)"
weight = 60
background = "white"

[header]
  image = "gallery/shallowwater.gif"
+++

The simulation covers a 11 km long section of the river Rhine and shows flooded areas
during an idealized flood event. Water flows from bottom to top.
The model has a discharge boundary condition at the inlet and a 
stage-discharge-curve as boundary condition at the outlet.

The simulation is based on the shallow water equations (a depth-averaged,
two-dimensional version of the Navier-Stokes equations).
The model uses a fully implicit time discretization and a finite volume discretization in space.
