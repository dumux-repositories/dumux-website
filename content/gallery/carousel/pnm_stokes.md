+++
date = "2020-07-01"
title = "Tracer transport from a pore network into a free-flow channel"
subtitle = "Coupled free-flow/pore-network model (single-phase flow)"
weight = 35
background = "white"

[header]
  image = "gallery/pnm_stokes.gif"
+++

A tracer component is injected at the bottom of a randomly generated pore-network.
It rises by advection and diffusion and finally reaches the adjacent free-flow channel, where the Navier-Stokes equations are solved.
Since no external flow is imposed in the channel, the tracer spreads almost symmetrically to the left and to the right.

More details on the fully monolithic coupled model and the simulation setup are given in [Weishaupt et al. (2019)](https://doi.org/10.1016/j.jcpx.2019.100011).
The source code is openly available at [dumux-pub/weishaupt2019a](https://git.iws.uni-stuttgart.de/dumux-pub/weishaupt2019a).
