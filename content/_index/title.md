+++
fragment = "item"
weight = 95
align = "right"

[asset]
  image = "logo.svg"

[[buttons]]
  text = "Getting started"
  url = "/docs/doxygen/master/getting-started.html"
  color = "primary"

[[buttons]]
  text = "Installation"
  url = "/docs/doxygen/master/installation.html"
  color = "primary"

[[buttons]]
  text = "Examples"
  url = "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/blob/master/examples/README.md"
  color = "primary"

[[buttons]]
  text = "Gallery"
  url = "/gallery"
  color = "primary"
+++

<h2>{{<dumux>}} - DUNE for Multi-{Phase, Component, Scale, Physics, …} flow and transport in porous media</h2>
<h5>An open-source simulator and research code in modern C++</h5>
<br>
