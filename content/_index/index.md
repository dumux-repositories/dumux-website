+++
url = "/"
title = "DuMux"
date = "2017-09-07"
description = "An open source porous media simulator"
+++

{{<dumux>}}, Dune for Multi-{Phase, Component, Scale, Physics, …} flow and transport in porous media: a free and open-source simulator and research code written in C++
