+++
fragment = "announcement"
disabled = true
weight = 96
background = "dark"
title = "DuMu<sup>x</sup> course announcement"
subtitle = "The next DuMu<sup>x</sup> course event will take place from 3-5 April 2023 in Stuttgart, Germany."

[[buttons]]
  text = "Learn more"
  url = "https://www.sfb1313.uni-stuttgart.de/integrated-research-training-group/short-courses/dumux-course-2023/"
  color = "primary"
+++
