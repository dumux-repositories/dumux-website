+++
title = "Python bindings"
weight = 73

[asset]
  icon = "fab fa-python"
+++

{{<dumux>}} has Python bindings.
The current state is experimental and
only a few features are accessible from Python (see [tests](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/tree/master/test/python?ref_type=heads)).
[Reach out to us](https://listserv.uni-stuttgart.de/mailman/listinfo/dumux) if you have a use case.
