+++
title = "Network and fracture models"
weight = 32
#disabled = true

[asset]
  image = "mod-frac.png"
+++

{{<dumux>}} can be used to solve systems
with embedded transport networks such as capillary-tissue coupling,
root-soil interaction, discrete fracture models.
