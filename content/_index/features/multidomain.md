+++
title = "Multi-domain simulations"
weight = 39

[asset]
  icon = "fas fa-puzzle-piece"
+++

{{<dumux>}} can couple problems posed on different domains.
The domains can touch or overlap, model different physics,
have different dimensions, different grids, different
discretization methods. The full system Jacobian
is approximated by numeric differentiation.
