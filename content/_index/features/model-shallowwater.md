+++
title = "Shallow-water model (2D)"
weight = 26
#disabled = true

[asset]
  image = "mod-shallow.png"
+++

{{<dumux>}} can solve the 2D shallow-water equations
including different bottom-friction models, basic turbulence models,
and surface drying/wetting on structured and unstructured grids.
Typical applications include river modelling and surface runoff.
