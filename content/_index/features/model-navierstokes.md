+++
title = "Navier-Stokes and Stokes equations"
weight = 16
#disabled = true

[asset]
  image = "mod-ff.png"
+++

{{<dumux>}} can solve laminar and turbulent
flow problems (DNS and RANS with various turbulence models).
The free flow solver can be coupled
to porous media or pore network models.
