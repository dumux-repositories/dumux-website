+++
title = "Tools for reproducible science"
weight = 70

[asset]
  icon = "fa fa-flask"
+++

{{<dumux>}} offers various scripts simplying
the creation of self-contained modules, installation scripts,
and Docker images to improve reproducibility
(also see [dumux-pub](https://git.iws.uni-stuttgart.de/dumux-pub/)).
We also support simulation meta data extraction.
