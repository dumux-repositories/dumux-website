+++
title = "FV/CVFE discretization methods"
weight = 40

[asset]
  icon = "fas fa-map"
+++

{{<dumux>}} offers a range of finite volume schemes (FV), such as
the cell-centered Tpfa and Mpfa methods, or the vertex-centered Box method
and other control-volume finite element schemes (CVFE).
