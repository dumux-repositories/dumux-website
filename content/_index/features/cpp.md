+++
title = "C++ code based on Dune"
weight = 41

[asset]
  icon = "fa fa-code"
+++

{{<dumux>}} is written in modern C++17 using generic programming techniques.
This makes the code fast and flexible. {{<dumux>}} is a
[DUNE](http://dune-project.org/) (Distributed and Unified Numerics Environment) module.
