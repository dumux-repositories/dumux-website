+++
title = "Parallel computing / HPC"
weight = 43

[asset]
  icon = "fa fa-bars"
+++

{{<dumux>}} supports parallel simulations using
a distributed memory parallelism based on MPI and shared memory parallelism
with several backends (OpenMP, TBB, Kokkos) out-of-the-box
for many models.
