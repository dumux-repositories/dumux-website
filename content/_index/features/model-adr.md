+++
title = "Advection-Diffusion-Reaction models"
weight = 13
#disabled = true

[asset]
  image = "mod-adr.png"
+++

{{<dumux>}} can be used to solve systems
of Advection-Diffusion-Reaction equations
in both porous and pure media.
Reactive transport models including phase separation models such as
the [Cahn-Hilliard equations](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/blob/master/examples/cahn_hilliard/README.md)
are also within the {{<dumux>}} feature set.
