+++
title = "Multi-phase multi-component (Darcy-scale)"
weight = 10

[asset]
  image = "mod-mm.png"
+++

{{<dumux>}} features compositional and immiscible, isothermal and non-isothermal, equilibrium and non-equlibrium models for multi-phase multi-component flow and transport in porous media with preimplemented but easily extensible constitutive relations, fluid properties.