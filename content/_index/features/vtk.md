+++
title = "VTK Output (Visualisation)"
weight = 45

[asset]
  icon = "fa fa-video"
+++

{{<dumux>}} supports output to VTK-based file formats
that are readily visualised e.g. using the open-source
software [ParaView](https://www.paraview.org/).
