+++
title = "Richards equation (flow in the unsaturated zone)"
weight = 19

[asset]
  image = "mod-richards.png"
+++

{{<dumux>}} can solve Richards equation
for flow in the unsaturated zone as well as
extensions of Richards equation considering
multiple components or adding
vapor diffusion in the gas phase.
