+++
title = "Pore-network models"
weight = 29
#disabled = true

[asset]
  image = "mod-pnm.png"
+++

{{<dumux>}} features single and multi-phase
pore-network models. Its specialty is dynamic
and implicit pore-network models.
The local rules are easily customizable.
A dual network model allows to couple
fluid flow and solid skeleton network models
(e.g. heat transfer, phase change).
