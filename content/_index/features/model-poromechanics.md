+++
title = "Solid and poro-mechanics models"
weight = 23
#disabled = true

[asset]
  image = "mod-mech.png"
+++

{{<dumux>}} can be used to solve solid mechanics problems
including linear elasticity, hyperelasticity, and poro-elasticity (Biot-like),
also in combination with fluid flow models.
