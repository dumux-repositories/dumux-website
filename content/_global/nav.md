+++
fragment = "nav"
#disabled = true
date = "2018-05-17"
weight = 0
background = "light"
search = true
sticky = true

[repo_button]
  url = "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux"
  text = "Get source code"
  icon = "fab fa-gitlab"

# Branding options
[asset]
  image = "logo.svg"
  text = "DuMu<sup>x</sup>"
+++
