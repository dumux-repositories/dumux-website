+++
fragment = "footer"
#disabled = true
date = "2016-09-07"
weight = 1200
background = "white"

menu_title = "Further Resources"

[asset]
  title = "DuMu<sup>x</sup>"
  image = "logo.svg"
  text = "Dumux"
  url = "/"
+++

#### {{<dumux>}}

Dune for Multi-{Phase, Component, Scale, Physics, ...} flow and transport in porous media:
a free and open-source simulator and research code written in C++
